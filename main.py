from nfc_lib.nfc import *
from nfc_lib.frame import *

nfc = Nfc()

try:
    print("touch card")
    target_uid = nfc.get_target_uid()

    print("uid: %x %x %x %x" % (target_uid[0], target_uid[1], target_uid[2], target_uid[3]))
    
    # 40: DataExchange
    print("authenticating")
   
    nfc.authenticate_A(address = 0x01, key = bytearray([0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]), uid = target_uid)
    
    # print("writing data")
    # nfc.write_16bytes_data(data = bytearray([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F]), address = 0x01)
    # nfc.write_4bytes_data(data = bytearray([0x04, 0x03, 0x02, 0x01]), address = 0x01)

    print("reading_data")
    nfc.read_data(address = 0x01)
    
except KeyboardInterrupt:
    print("you hit ctrl-c")
    nfc.reset_i2c()
    