from nfc_lib.defaults import *
from nfc_lib.util import *

class Frame:
    def __init__(
        self,
        fid = 0xD4,
        preamble = DEFAULT_PREAMBLE,
        start_code_1 = START_CODE_1,
        start_code_2 = START_CODE_2,
        data = bytearray(),
        postamble = DEFAULT_POSTAMBLE):
    
        self._preamble = preamble
        self._start_code_1 = START_CODE_1
        self._start_code_2 = START_CODE_2
        self._data = data
        self._length = self.get_length()
        self._length_cs = self.get_length_checksum()
        self._fid = fid        
        self._data_cs = self.get_data_checksum()
        self._postamble = postamble
    
    def is_ack(self):
        if self._preamble == DEFAULT_PREAMBLE:
            if self._start_code_1 == START_CODE_1:
                if self._start_code_2 == START_CODE_2:
                    if self._length == 0x01:
                        if self._length_cs == 0xFF:
                            if self._fid == 0x00:
                                    return True
        return False
    def is_error(self):
        if(self._fid == 0x7f):
            return True
        return False
    
    @staticmethod
    def is_valid_response(response):
        if (response[0][0] & 0x01) == 0x01:
            if response[0][1] == DEFAULT_PREAMBLE:
                if response[0][2] == START_CODE_1:
                    if response[0][3] == START_CODE_2:
                        return True

        return False

    @staticmethod
    def from_response(response):
        if Frame.is_valid_response(response) is not True:
            raise ValueError("Invalid Response")

        response_length = response[0][4] + 1
        data = bytearray(
            response[0][7:7+ response_length - 2])

        return Frame(
            preamble=response[0][1],
            start_code_1=response[0][2],
            start_code_2=response[0][3],
            fid=response[0][6],
            data=data,
            postamble=response[0][7 + response_length + 2])

    def to_byte_array(self):
        byte_array = bytearray()

        byte_array.append(self._preamble)
        byte_array.append(self._start_code_1)
        byte_array.append(self._start_code_2)
        byte_array.append(self._length)
        byte_array.append(self._length_cs)
        byte_array.append(self._fid)

        for byte in self._data:
            byte_array.append(byte)
        
        byte_array.append(self._data_cs)
        byte_array.append(self._postamble)

        return (byte_array)

    def printable(self):
        prnt = "[ %x | %x | %x | %d | %d | %x --" % (
            self._preamble, 
            self._start_code_1, 
            self._start_code_2,
            self._length,
            self._length_cs,
            self._fid)        
       
        for x in self._data:
            if x < 16:
                prnt = prnt + "0%x" % x
            else:
                prnt = prnt + "%x" % x

        prnt  = prnt + " -- %x | %x ]" % (self._data_cs, self._postamble)
        return prnt

    def to_string(self):
        ret = ''
        for x in self._data:
            ret = ret + Util.translate(x)
        
        return ret

    def get_length(self):
        return len(self._data) + 1
    
    def get_length_checksum(self):
        return (~self._length & 0xFF) + 0x01
    
    def get_data_checksum(self):
        byte_array = bytearray()

        for byte in self._data:
            byte_array.append(byte)
        
        byte_array.append(self._fid)

        inverse = (~sum(byte_array) & 0xFF) + 0x01

        if inverse > 255:
            inverse = inverse - 255
        
        return inverse

    def get_application_label(self):
        fci_len = self._data[3]
        fci = self._data[2 : 2+ fci_len]
        aid_len = fci[3]
        # aid = app[4:4+id_len]
        
        label_len = fci[7 + aid_len]

        label = fci[7 + aid_len + 1 : 7 + aid_len + 1 + label_len]

        return label
