from time import sleep
import logging
from nfc_lib.frame import *
from nfc_lib.defaults import *
from quick2wire.i2c import I2CMaster, reading, writing
from nfc_lib.util import *

DEFAULT_DELAY = 0.5

class Nfc:
    logger = None

    def __init__(self,log_enabled = False, log_level = logging.DEBUG, address= PI_I2C_ADDRESS, i2c_channel=PI_I2C_CHANNEL):
        self.logger = logging.getLogger()
        self.logger.propagate = log_enabled

        if self.logger.propagate:
            self.logger.setLevel(log_level)
        
        self.address = address
        self.i2c_channel = i2c_channel
        self.PN532 = I2CMaster(self.i2c_channel)

        # SAM configuration -> normal mode: sam not used
        sam_config = Frame(data=bytearray([0x14, 0x01, 0x01, 0x00]))
        logging.debug("setting SAM configuration")
        self.send_command(sam_config)

        if self.receive_ack() == False:
            raise Exception("host didn't respond on SAM configuration")

    def send_command(self, frame):
        while True:
            try:
                logging.debug("send_command...........Sending.")
                sleep(DEFAULT_DELAY)
                self.PN532.transaction(
                    writing(self.address, frame.to_byte_array()))

                logging.debug(frame.printable())
                logging.debug("send_command...........Sent.")
            except Exception as ex:
                logging.debug(ex)
                "Send exc"
                self.reset_i2c()
                sleep(DEFAULT_DELAY)
            else:
                return True
    
    def read_response(self):
        logging.debug("readResponse...")
        response = bytearray([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])

        while True:
            try:
                # logging.debug("Reading response")

                sleep(DEFAULT_DELAY)
                response = self.PN532.transaction(
                    reading(self.address, 255))
                
                logging.debug(response)
            except Exception:
                logging.debug("no response...")
                pass
            else:
                try:
                    fr_res = Frame.from_response(response)
                except ValueError as ex:
                    pass
                except Exception as ex:
                    logging.debug(ex)
                    logging.debug(ex.args)
                    pass
                else:
                    return fr_res
    
    def receive_ack(self):

        response = self.read_response()
        
        while True:
            if response.is_ack():
                logging.debug("ack received")
                return True
            else:
                logging.debug("expected ack not received")
                pass
        
    def reset_i2c(self):
        """Reset the I2C communication connection."""
        logging.debug("I2C Reset...")

        self.PN532.close()
        del self.PN532
        self.PN532 = I2CMaster(self.i2c_channel)

        logging.debug("I2C Reset............Created.")

    def write_16bytes_data(self, data, address, target = 0x01):
        data_to_write=bytearray([0x40, target, 0xA0, address])
        
        for x in data:
            data_to_write.append(x)

        self.send_command(Frame(data = data_to_write))
        
        if self.receive_ack() == False:
            raise Exception("host didn't respond on DataExchange for Write data")

        res = self.read_response()
        
        if res.is_error():
            logging.error("Error response from host on writing")
            return False

        print(res.printable())
        return True
    
    def write_4bytes_data(self, data, address, target = 0x01):
        data_to_write=bytearray([0x40, target, 0xA2, address])
        
        for x in data:
            data_to_write.append(x)

        self.send_command(Frame(data = data_to_write))
        
        if self.receive_ack() == False:
            raise Exception("host didn't respond on DataExchange for Write data")

        res = self.read_response()
        
        if res.is_error():
            logging.error("Error response from host on writing")
            return False

        print(res.printable())
        return True

    def read_data(self, address, target = 0x01):
        # 40: DataExchange
        read_cmd = Frame(data=bytearray([0x40, target, 0x30, address]))
        
        self.send_command(read_cmd)
        
        if self.receive_ack() == False:
            raise Exception("host didn't respond on DataExchange for Read data")
        
        read = self.read_response()

        return read

    def authenticate_A(self, address, key, uid, target = 0x01):
         # 40: DataExchange
        data =  bytearray([0x40, target, 0x60, address])
        
        for k in key:
            data.append(k)
        
        for u in uid:
            data.append(u)

        dataex_cmd = Frame(data=data)

        # print(dataex_cmd.printable())
        self.send_command(dataex_cmd)
        
        if self.receive_ack() == False:
            raise Exception("host didn't respond on DataExchange for Auth")
        
        dataex_response = self.read_response()

        logging.info(dataex_response.printable())

        if len(dataex_response._data) > 2 or dataex_response._data[1] == 0x14:
            self.logger.info("auth error")
            return False
        if dataex_response._data[1] == 0x00:
            self.logger.info("auth success")
            return True

    def get_target_uid(self):
        # 4A: ListPassiveTargets
        cmd_list_passive_trgt = Frame(data=bytearray([0x4A, 0x01, 0x00]))
        self.send_command(cmd_list_passive_trgt)

        if self.receive_ack() == False:
            raise Exception("host didn't respond on ListPassiveTarget")
        
        res = self.read_response()

        logging.info(res.printable())
        idLen = res._data[6]

        return (res._data[7 : 7 + idLen])        

    def test_card(self, aid):
        data = bytearray([0x40,0x01,0x00,0xA4,0x04,0x00,0x07])
        
        for a in aid:
            data.append(a)

        self.send_command(Frame(data=data))
        
        if self.receive_ack() == False:
            raise Exception("host didn't respond on DataExchange for Auth")
        
        response = self.read_response()

        logging.info(response.printable())

        return response

    def get_aid_from_pse(self):
        aid = 0
        pse = Frame(data=bytearray([0x40, 0x01, 0x00, 0xA4, 0x04, 0x00, 0x0E, 0x32, 0x50, 0x41, 0x59, 0x2E, 0x53, 0x59, 0x53, 0x2E, 0x44, 0x44, 0x46, 0x30, 0x31]))
        self.send_command(pse)
        if self.receive_ack() == False:
            raise Exception("host didn't respond on DataExchange for pse")

        pse_res = self.read_response()

        sleep(0.05)
        logging.info("pse_file:")
        logging.info(pse_res.printable())

        el = Element.from_bytearray(pse_res._data[2:])
        
        print("code: %x" % el.code)
        
        elements = el.inner_elements()

        for e in elements:
            print("inner code: %x" % e.code)
            print("data: ")
            print(e.data)

            for inner in e.inner_elements():
                print("inner code: %x" % inner.code)
                # print("internal data: ")
                # print(inner.data)

        if len(pse_res._data) > 2 and pse_res._data[2] == 0x6F:
            logging.info("pse file found")

        fci_template_cd_index = pse_res._data[5] + 6
        fci_template_cd = pse_res._data[fci_template_cd_index]
    
        if fci_template_cd == 0xA5:
            if pse_res._data[fci_template_cd_index + 2] == 0xBF:
                if pse_res._data[fci_template_cd_index + 3] == 0x0C:
                    if pse_res._data[fci_template_cd_index + 5] == 0x61:
                        if pse_res._data[fci_template_cd_index + 7] == 0x4F:
                            aid_length = pse_res._data[fci_template_cd_index + 8]
                            
                            i0= fci_template_cd_index + 9
                            i1 = fci_template_cd_index + aid_length + 9
                            aid = pse_res._data[i0 : i1]
                            
                            logging.info(aid)
        return aid

class Element:
    def __init__(self, code, data):
        self.code = code
        self.data = data

    @staticmethod
    def from_bytearray(array):
        length = array[1]
        return Element(code=array[0], data = array[2 : 2 + length])

    def inner_elements(self):
        index = 0
        ret = []
        go = True
        data_length = len(self.data)

        print("length: %d" % data_length)

        while index < data_length:
            if Util.is_known_code(self.data[index]):
                length = self.data[index + 1]
                ret.append(Element(code = self.data[index], data =self.data[index + 2 : index + 2 + length]))
                index = index + 2 + length
            elif Util.is_known_code(self.data[index:index+1]):
                index = index + 1
                length = self.data[index + 1]
                ret.append(Element(code = self.data[index], data =self.data[index + 2 : index + 2 + length]))
                index = index + 2 + length
        return ret
