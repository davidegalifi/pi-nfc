class Util:
    def __init__(self):
        pass
    @staticmethod
    def is_known_code(code):
        
        known_codes = [
            0x64, 
            0x84, 
            0xA5,
            0x61,
            0x4F,
            0x87,
            0x50,
            0x90,
            [0xBF, 0x0C]]

        if code in known_codes:
            return True

        return False

    @staticmethod
    def translate(hex_value):
        ret = ''
        if hex_value == 0x41:
            ret = 'A'
        elif hex_value == 0x42:
            ret = 'B'
        elif hex_value == 0x43:
            ret = 'C'
        elif hex_value == 0x44:
            ret = 'D'
        elif hex_value == 0x45:
            ret = 'E'
        elif hex_value == 0x46:
            ret = 'F'
        elif hex_value == 0x47:
            ret = 'G'
        elif hex_value == 0x48:
            ret = 'H'
        elif hex_value == 0x49:
            ret = 'I'
        elif hex_value == 0x4A:
            ret = 'J'
        elif hex_value == 0x4B:
            ret = 'K'
        elif hex_value == 0x4C:
            ret = 'L'
        elif hex_value == 0x4D:
            ret = 'M'
        elif hex_value == 0x4E:
            ret = 'N'
        elif hex_value == 0x4F:
            ret = 'O'
        elif hex_value == 0x50:
            ret = 'P'
        elif hex_value == 0x51:
            ret = 'Q'
        elif hex_value == 0x52:
            ret = 'R'
        elif hex_value == 0x53:
            ret = 'S'
        elif hex_value == 0x54:
            ret = 'T'
        elif hex_value == 0x55:
            ret = 'U'
        elif hex_value == 0x56:
            ret = 'V'
        elif hex_value == 0x57:
            ret = 'W'
        elif hex_value == 0x58:
            ret = 'X'
        elif hex_value == 0x59:
            ret = 'Y'
        elif hex_value == 0x5A:
            ret = 'Z'
        elif hex_value == 0x61:
            ret = 'a'
        elif hex_value == 0x62:
            ret = 'b'
        elif hex_value == 0x63:
            ret = 'c'
        elif hex_value == 0x64:
            ret = 'd'
        elif hex_value == 0x65:
            ret = 'e'
        elif hex_value == 0x66:
            ret = 'f'
        elif hex_value == 0x67:
            ret = 'g'
        elif hex_value == 0x68:
            ret = 'h'
        elif hex_value == 0x69:
            ret = 'i'
        elif hex_value == 0x6a:
            ret = 'j'
        elif hex_value == 0x6b:
            ret = 'k'
        elif hex_value == 0x6c:
            ret = 'l'
        elif hex_value == 0x6d:
            ret = 'm'
        elif hex_value == 0x6e:
            ret = 'n'
        elif hex_value == 0x6f:
            ret = 'o'
        elif hex_value == 0x70:
            ret = 'p'
        elif hex_value == 0x71:
            ret = 'q'
        elif hex_value == 0x72:
            ret = 'r'
        elif hex_value == 0x73:
            ret = 's'
        elif hex_value == 0x74:
            ret = 't'
        elif hex_value == 0x75:
            ret = 'u'
        elif hex_value == 0x76:
            ret = 'v'
        elif hex_value == 0x77:
            ret = 'w'
        elif hex_value == 0x78:
            ret = 'x'
        elif hex_value == 0x79:
            ret = 'y'
        elif hex_value == 0x7a:
            ret = 'z'
        


        else:
            ret = '-'
        
        return ret
            