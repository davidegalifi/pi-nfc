var gulp = require('gulp');
var scpClient = require('scp2');
var scp = require('gulp-scp2');

gulp.task('default', ['watch']);

gulp.task('ssh-copy', function (cb) {
    console.log('ssh copy start...');

    return gulp.src('**/*.py')
        .pipe(scp({
            host: '192.168.1.207',
            username: 'pi',
            password: 'raspberry',
            dest: '/home/pi/run'
        }))
        .on('error', function(err) {
            console.log(err);
    }); 
});

gulp.task('watch', function (cb) {
    gulp.watch(['./**/*.py'], ['ssh-copy']);
});