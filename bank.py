from nfc_lib.nfc import *
from nfc_lib.frame import *
import logging
import time

LOGGING_ENABLED = True
LOG_LEVEL = logging.WARNING

logger = logging.getLogger()
logger.propagate = LOGGING_ENABLED

if logger.propagate:
    logger.setLevel(LOG_LEVEL)

nfc = Nfc(LOGGING_ENABLED, LOG_LEVEL)

try:
    print("touch card")
    
    target_uid = nfc.get_target_uid()
    
    if len(target_uid) == 4:
        print("uid: %x %x %x %x" % (target_uid[0], target_uid[1], target_uid[2], target_uid[3]))
    if len(target_uid) == 7:
        print("uid: %x %x %x %x %x %x %x" % (target_uid[0], target_uid[1], target_uid[2], target_uid[3], target_uid[4], target_uid[5], target_uid[6]))    

    visa = bytearray([0xA0,0x00,0x00,0x00,0x03,0x10,0x10,0x00])
    v_pay = bytearray([0xA0,0x00,0x00,0x00,0x03,0x20,0x10, 0x00])
    maestro = bytearray([0xA0,0x00,0x00,0x00,0x04,0x30,0x60, 0x00])
    mastercard = bytearray([0xA0,0x00,0x00,0x00,0x04,0x10,0x10, 0x00])

    card_aids = [v_pay, maestro, mastercard, visa]
    names = ['v_pay', 'maestro', 'mastercard', 'visa']

    aid = nfc.get_aid_from_pse()
    
    if aid != 0:
        test_card_res = nfc.test_card(aid)
        if test_card_res._data[2] == 0x6F:
                logging.info(test_card_res.printable())

                print("Application Label: ")
                logging.info(test_card_res.get_application_label())
    else:
        for i in range(0, len(card_aids)):    
            test_card_res = nfc.test_card(card_aids[i])
            
            if test_card_res._data[2] == 0x6F:
                print(names[i] + ' found')
                logging.info(test_card_res.printable())

                print("Application Label: ")
                logging.info(test_card_res.get_application_label())
            
            sleep(0.01)
    
    cmd = Frame(data=bytearray([0x40, 0x01, 0x80, 0xa8, 0x00, 0x00, 0x23, 0x83, 0x21, 0xF3,0x20,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x55,0x4B,0x00,0x00,0x00,0x00,0x00,0x08,0x26,0x11,0x0B,0x1D,0x00,0x01,0x02,0x03,0x04,0x00]))
    nfc.send_command(cmd)

    if nfc.receive_ack() == False:
        raise Exception("host didn't respond on cmd")
        
    rrr = nfc.read_response()

    logging.info(rrr.printable())

except KeyboardInterrupt:
    print("you hit ctrl-c")
    nfc.reset_i2c()