from nfc_lib.nfc import *
from nfc_lib.frame import *
import logging
import time

nfc = Nfc(log_enabled = True)

print("touch card")

data_bytes = bytearray([0x4A, 0x01, 0x03, 0x01])
list_passive_trgt = Frame(data=data_bytes)
nfc.send_command(list_passive_trgt)

if nfc.receive_ack() == False:
    raise Exception("host didn't respond on ListPassiveTarget")

res = nfc.read_response()
print(res.printable())